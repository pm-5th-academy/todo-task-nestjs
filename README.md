
## Todo Task -  Using Nest framework

Project: Todo Task developed by Mark(Dimang)

## Description
Using MySQL database with sequelize.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
