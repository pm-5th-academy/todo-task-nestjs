import { CreatedAt, UpdatedAt, Column, Model, Table, PrimaryKey, Default, Unique, ForeignKey, BelongsTo, IsUUID, HasMany } 
  from 'sequelize-typescript';
import { User } from '../users/user.model';
import { IsEmpty, IsString } from 'class-validator';
import { Task } from '../tasks/task.model';

@Table
export class Todo extends Model<Todo> {
  @IsUUID(4)
  @Unique
  @PrimaryKey
  @Column
  id: string;

  @Default('ACTIVE')
  @Column
  status: string;

  @Column
  description: string;

  @CreatedAt
  createdAt: Date;
 
  @UpdatedAt
  updatedAt: Date;


  @ForeignKey(() => User)
  @Column
  userId: string;

  @BelongsTo(() => User)
  user: User;

  @HasMany(() => Task)
  tasks: Task[];
}
