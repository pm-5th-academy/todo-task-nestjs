import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Todo } from './todo.model';

@Injectable()
export class TodosService {
  constructor(@Inject('TODOS_REPOSITORY') private readonly todosRepository: typeof Todo) {}

  async findAll(): Promise<Todo[]> {
    return this.todosRepository.findAll<Todo>();
  }
}
