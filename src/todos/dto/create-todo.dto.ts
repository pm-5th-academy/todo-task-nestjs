
export class CreateTodoDto {
  readonly description: string;
  readonly userId: string;
  readonly status: string;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
