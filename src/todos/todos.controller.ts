import { Controller, Get } from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo.dto';
import { TodosService as TodosService } from './todos.service';
import { Todo } from './todo.model';

@Controller('todos')
export class TodosController {
  constructor(private readonly todosService: TodosService) {}

  @Get()
  async findAll(): Promise<Todo[]> {
    return this.todosService.findAll();
  }
}

