import { Sequelize } from 'sequelize-typescript';
import { User } from '../users/user.model';
import { Todo } from '../todos/todo.model';
import { Task } from '../tasks/task.model';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        operatorsAliases: false,
        dialect: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: '',
        database: 'user_todo',
      });
      sequelize.addModels([User, Todo, Task]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
