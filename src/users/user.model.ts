import { CreatedAt, UpdatedAt, Column, Model, Table, DataType, PrimaryKey, IsUUID, Default, Unique, HasMany } from 'sequelize-typescript';
import { Todo } from '../todos/todo.model';
import { IsEmail, IsNotEmpty, IsNumber, IsString, MinLength } from 'class-validator';

@Table
export class User extends Model<User> {
  @IsUUID(4)
  @Unique
  @PrimaryKey
  @Column
  id: string;

  @Column
  firstName: string;

  @Column
  lastName: string;

  @Column
  age: number;

  @Unique
  @Column
  email: string;

  @Column
  password: string;

  @Column
  @CreatedAt
  createdAt: Date;

  @Column
  @UpdatedAt
  updatedAt: Date;

  @Default('ACTIVE')
  @Column
  status: string;

  @HasMany(() => Todo)
  todos: Todo[];
}
