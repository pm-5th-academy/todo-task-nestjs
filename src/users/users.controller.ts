import { Controller, Get, Post, Body, Res, HttpStatus, Req, Param, Put, Delete, Query } from '@nestjs/common';
import { Response } from 'express';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { User } from './user.model';
import { CreateTodoDto } from '../todos/dto/create-todo.dto';
import { async } from 'rxjs/internal/scheduler/async';
import { CreateTaskDto } from '../tasks/dto/create-task.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async create(@Body() createUserDto: CreateUserDto, @Res() res: Response) {
    await this.usersService.create(createUserDto, res);
  }

  @Post('/login')
  async login(@Body() createUserDto: CreateUserDto, @Res() res: Response) {
    await this.usersService.login(createUserDto, res);
  }

  @Get()
  async findAll(@Query() query, @Res() res: Response): Promise<User[]> {
    return this.usersService.findAll(query, res);
  }

  @Get('/:userId')
  async getUser(@Param('userId') userId, @Res() res: Response) {
    await this.usersService.getUser(userId, res);
  }

  @Put('/:userId')
  async updateUser(@Body() createUserDto: CreateUserDto, @Param('userId') userId, @Res() res: Response) {
    await this.usersService.updateUser(createUserDto, userId, res);
  }

  @Delete('/:userId')
  async deleteUser(@Param('userId') userId, @Res() res: Response) {
    await this.usersService.deleteUser(userId, res);
  }

  @Post('/:userId/todos')
  async createTodo(@Body() createTodoDto: CreateTodoDto, @Body() user: User, @Param('userId') userId, @Res() res: Response) {
    await this.usersService.createTodo(createTodoDto, user, userId, res);
  }

  @Get('/:userId/todos/:todoId')
  async getTodo(@Param('userId') userId, @Param('todoId') todoId, @Res() res: Response) {
    await this.usersService.getTodo(userId, todoId, res);
  }

  @Put('/:userId/todos/:todoId')
  async updateTodo(@Body() createTodoDto: CreateTodoDto, @Param('userId') userId, @Param('todoId') todoId, @Res() res: Response) {
    await this.usersService.updateTodo(createTodoDto, userId, todoId, res);
  }

  @Delete('/:userId/todos/:todoId')
  async deleteTodo(@Param('userId') userId, @Param('todoId') todoId, @Res() res: Response) {
    await this.usersService.deleteTodo(userId, todoId, res);
  }

  @Post('/:userId/todos/:todoId/tasks')
  async createTask(@Body() createTaskDto: CreateTaskDto, @Param('userId') userId, @Param('todoId') todoId, @Res() res: Response) {
    await this.usersService.createTask(createTaskDto, userId, todoId, res);
  }

  @Get('/:userId/todos/:todoId/tasks/:taskId')
  async getTask(@Param('userId') userId, @Param('todoId') todoId, @Param('taskId') taskId, @Res() res: Response) {
    await this.usersService.getTask(userId, todoId, taskId, res);
  }

  @Put('/:userId/todos/:todoId/tasks/:taskId')
  async updateTask(
    @Body() createTaskDto: CreateTaskDto,
    @Param('userId') userId,
    @Param('todoId') todoId,
    @Param('taskId') taskId,
    @Res() res: Response
  ) {
    await this.usersService.updateTask(createTaskDto, userId, todoId, taskId, res);
  }

  @Delete('/:userId/todos/:todoId/tasks/:taskId')
  async deleteTask(@Param('userId') userId,@Param('todoId') todoId, @Param('taskId') taskId,@Res() res:Response){
    await this.usersService.deleteTask(userId, todoId, taskId, res);
  }
}
