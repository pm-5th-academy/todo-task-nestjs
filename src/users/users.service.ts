import * as bcrypt from 'bcrypt';
import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Response } from 'express';
import { validate } from 'indicative/validator';
import { User } from './user.model';
import { Todo } from '../todos/todo.model';
import { CreateTodoDto } from '../todos/dto/create-todo.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { regex } from 'regex';
import * as uuid from 'uuid/v4';
import { isRegExp } from 'util';
import { Sequelize } from 'sequelize-typescript';
import { Task } from '../tasks/task.model';
import { CreateTaskDto } from '../tasks/dto/create-task.dto';
const saltRounds = 10;

@Injectable()
export class UsersService {
  constructor(@Inject('USERS_REPOSITORY') private readonly usersRepository: typeof User) {}

  async create(createUserDto: CreateUserDto, res: Response): Promise<User> {
    try {
      // TODO: add validation using indicative
      const { firstName, lastName, email, age, password } = createUserDto;
      let hash;
      if (password) {
        hash = bcrypt.hashSync(password, saltRounds);
      }
      const isExistingUser = await User.findOne({ where: { email } });
      if (isExistingUser) return res.status(HttpStatus.BAD_REQUEST).json('Email already exist');
      const id = uuid();
      const user = new User({ id, firstName, lastName, email, age, password: hash });
      await user.save();
      res.status(HttpStatus.CREATED).json('Successful Created user');
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async login(createUserDto: CreateUserDto, res: Response) {
    try {
      const { email, password } = createUserDto;
      const user = await User.findOne({ where: { email } });
      if (!user) return res.status(HttpStatus.NOT_FOUND).json('Email not found');
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) return res.status(HttpStatus.BAD_REQUEST).json('Incorrect password');
      return res.status(200).json('Successful sign in');
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  // TODO Add search , sort, limit, offset
  // GET /users?limit=10&offset=1
  // GET /users?search=Something
  // GET /users?sortBy=firstname:asc or desc
  // GET /users?sortBy=lastname:asc or desc
  // GET /users?sortBy=createdAt:desc
  async findAll(query, res: Response): Promise<User[]> {
    try {
      const { limit = 10, offset = 0, search = '', sortBy } = query;
      const sort = ['createdAt', 'DESC']; // Default sort
      const Op = Sequelize.Op;
      if (sortBy) {
        const parts = sortBy.split(':');
        sort[0] = parts[0];
        sort[1] = parts[1];
        console.log(sort);
      }

      const users = await User.findAll({
        where: {
          firstName: { [Op.regexp]: '[[:<:]]' + search },
          status: 'ACTIVE'
        },
        order: [sort],
        offset,
        limit
      });
      if (!users) return res.status(HttpStatus.NOT_FOUND).json('Not Found');
      return res.status(HttpStatus.OK).json(users);
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async getUser(userId: string, res: Response): Promise<User> {
    try {
      const user = await User.findByPk(userId);
      if (!user) return res.status(404).json('User not found');
      return res.status(HttpStatus.OK).json(user);
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async updateUser(createUserDto: CreateUserDto, userId: string, res: Response): Promise<User> {
    try {
      const user = await User.findByPk(userId);
      console.log(user);
      if (!user) {
        res.status(HttpStatus.NOT_FOUND).json('User Not Found');
      }
      const rules = {
        email: 'email',
        password: 'min:4',
        age: 'number',
        firstName: 'string | min:2',
        lastName: 'string | min:2'
      };
      const messages = {
        'email.email': 'Please enter a valid email address',
        'password.min': 'Password is too short'
      };
      await validate(createUserDto, rules, messages);
      // emptyOrSpace
      for (const key in createUserDto) {
        if (createUserDto[key] === '' || createUserDto[key] === null || createUserDto[key].match(/^ *$/) !== null) {
          return res.status(400).json(key + ' is required ');
        }
      }

      const { password } = createUserDto;
      if (password) {
        const hash = bcrypt.hashSync(password, saltRounds);
        createUserDto.password = hash;
      }
      await user.update(createUserDto, { where: { id: userId } });
      res.status(HttpStatus.OK).json('Successfully Updated User');
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async deleteUser(userId: string, res: Response): Promise<User> {
    try {
      const user = await User.findByPk(userId);
      console.log(user);
      if (!user) {
        res.status(HttpStatus.NOT_FOUND).json('User Not Found');
      }
      const deleteUser = await user.update({ status: 'DELETE' });
      if (deleteUser) return res.status(HttpStatus.OK).json('Successful Deleted User');
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async createTodo(createTodoDto: CreateTodoDto, user: User, userId: string, res: Response): Promise<Todo> {
    try {
      // TODO: Add validation
      const user = await User.findByPk(userId);
      console.log('user' + user.firstName);
      if (!user) return res.status(HttpStatus.NOT_FOUND).json('User not found');
      const id = uuid();
      const todo = new Todo({
        id,
        ...createTodoDto,
        userId
      });
      await todo.save();
      res.status(HttpStatus.CREATED).json('Successful Created Todo');
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async getTodo(userId: string, todoId: string, res: Response): Promise<Todo> {
    try {
      const user = await User.findByPk(userId);
      if (!user) return res.status(HttpStatus.NOT_FOUND).json('User not found');
      const todo = await Todo.findOne({
        where: {
          userId,
          status: 'ACTIVE'
        }
      });
      if (!todo) return res.status(HttpStatus.NOT_FOUND).json('Todo not found');
      return res.status(HttpStatus.OK).json(todo);
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async updateTodo(createTodoDto: CreateTodoDto, userId: string, todoId: string, res: Response): Promise<Todo> {
    try {
      const todo = await Todo.findByPk(todoId);
      if (!todo) return res.status(HttpStatus.NOT_FOUND).json('Todo Not Found');
      
      const rules = {
        description: 'string|min:4'
      };
      const messages = {
        'description.string': 'Please enter a valid description in text'
      };
      await validate(createTodoDto, rules, messages, { removeAdditional: true });
      // TODO: convert as function isEmptyOrSpace
      for (const key in createTodoDto) {
        if (createTodoDto[key] === '' || createTodoDto[key] === null || createTodoDto[key].match(/^ *$/) !== null) {
          return res.status(400).json(key + ' is required ');
        }
      }

      const success = await todo.update(createTodoDto);
      if (!success) return res.status(HttpStatus.BAD_REQUEST).json('Unable to update');
      res.status(HttpStatus.OK).json('Successful Updated Todo');
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async deleteTodo(userId: string, todoId: string, res: Response): Promise<Todo> {
    try {
      const todo = await Todo.findOne({
        where: {
          userId,
          id: todoId
        }
      });
      if (!todo) res.status(HttpStatus.NOT_FOUND).json('Todo Not Found');
      const deleteTodo = await todo.update({ status: 'DELETE' });
      if (deleteTodo) return res.status(HttpStatus.OK).json('Successful Deleted Todo');
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async createTask(createTaskDto: CreateTaskDto, userId: string, todoId: string, res: Response): Promise<Task> {
    try {
      // TODO Add validition using indicative
      // TODO convert to function checkTodo for validate existed todo with userid
      const user = await User.findByPk(userId);
      if (!user) return res.status(HttpStatus.NOT_FOUND).json('User not found');
      const todo = await Todo.findOne({
        where: {
          userId,
          id: todoId
        }
      });
      if (!todo) return res.status(HttpStatus.NOT_FOUND).json('Todo not found');

      const id = uuid();
      const task = new Task({
        id,
        ...createTaskDto,
        todoId
      });
      await task.save();
      res.status(HttpStatus.CREATED).json('Successful Created Task');
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async getTask(userId: string, todoId: string, taskId: string, res: Response): Promise<Task> {
    try {
      const user = await User.findByPk(userId);
      if (!user) return res.status(HttpStatus.NOT_FOUND).json('User not found');
      const todo = await Todo.findOne({
        where: {
          userId,
          status: 'ACTIVE'
        }
      });
      if (!todo) return res.status(HttpStatus.NOT_FOUND).json('Todo not found');

      const task = await Task.findOne({
        where: {
          todoId,
          status: 'ACTIVE'
        }
      });
      return res.status(HttpStatus.OK).json(task);
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async updateTask(createTaskDto: CreateTaskDto, userId: string, todoId: string, taskId: string, res: Response): Promise<Task> {
    try {
      const task = await Task.findByPk(taskId);
      if (!task) return res.status(HttpStatus.NOT_FOUND).json('Task Not Found');

      // TODO: Add validation function
      const success = await task.update(createTaskDto);
      if (!success) return res.status(HttpStatus.BAD_REQUEST).json('Unable to update');
      res.status(HttpStatus.OK).json('Successful Updated Todo');
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  async deleteTask(userId: string, todoId: string, taskId: string, res:Response): Promise<Task>{
    try {
      const todo = await Todo.findOne({
        where: {
          userId,
          id: todoId
        }
      });
      if (!todo) return res.status(HttpStatus.NOT_FOUND).json('Todo Not Found');
      const task = await Task.findOne({
        where: {
          todoId,
          id: taskId
        }
      });
      if(!task) return res.status(HttpStatus.NOT_FOUND).json('Task Not found')
      const success = await task.update({ status: 'DELETE' });
      if(success) return res.status(HttpStatus.OK).json('Successful Deleted Task');
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
    }
  }
}
