export class CreateUserDto {
  readonly firstName: string;
  readonly lastName: string;
  readonly age: number;
  readonly email: string;
  password: string;
  readonly status: string;

  readonly creationDate: Date;
  readonly updatedOn: Date;
}
