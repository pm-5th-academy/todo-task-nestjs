import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { TodosService } from './todos/todos.service';
import { TodosController } from './todos/todos.controller';
import { TodosModule } from './todos/todos.module';
import { TasksController } from './tasks/tasks.controller';
import { TasksModule } from './tasks/tasks.module';

@Module({
  imports: [UsersModule, TodosModule, TasksModule],
})
export class ApplicationModule {}
