import { Module } from '@nestjs/common';
import { TasksController as TasksController } from './tasks.controller';
import { TasksService as TasksService } from './tasks.service';
import { tasksProviders as tasksProviders } from './tasks.providers';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [TasksController],
  providers: [TasksService, ...tasksProviders],
})
export class TasksModule {}
