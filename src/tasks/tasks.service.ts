import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Task } from './task.model';

@Injectable()
export class TasksService {
  constructor(@Inject('TASKS_REPOSITORY') private readonly tasksRepository: typeof Task) {}

  async findAll(): Promise<Task[]> {
    return this.tasksRepository.findAll<Task>();
  }
}
