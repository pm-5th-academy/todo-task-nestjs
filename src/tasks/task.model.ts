import { CreatedAt, UpdatedAt, Column, Model, Table, PrimaryKey, Default, Unique, ForeignKey, BelongsTo, IsUUID } from 'sequelize-typescript';
import { Todo } from '../todos/todo.model';

@Table
export class Task extends Model<Task> {
  @IsUUID(4)
  @Unique
  @PrimaryKey
  @Column
  id: string;

  @Column
  taskName: string;

  @Default('ACTIVE')
  @Column
  status: string;

  @CreatedAt
  createdAt: Date;
 
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Todo)
  @Column
  todoId: string;

  @BelongsTo(() => Todo)
  todo: Todo;
}
