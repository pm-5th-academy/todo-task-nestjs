
export class CreateTaskDto {
  readonly taskName: string;
  readonly dueDate: Date;
  readonly status: string;
  readonly todoId: string;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
